\documentclass[letterpaper,12pt]{article}
\pdfpagewidth=\paperwidth
\pdfpageheight=\paperheight
\newcommand{\issolution}{false}

%% Put your name here
\newcommand{\scribe}{Shane Carr}
%% Lecture number here
\newcommand{\NUM}{16}

\usepackage{wrapfig}

\input{macros.tex}

\begin{document}
\lecture{\NUM}{Linearizability and Sequential Consistency}{03/19/2015}{\scribe}

{
\raggedright
Outline of this lecture:
\begin{closeenum}
\item Final Project Logistics
\item Sequential Consistency
\item Linearizability
\item Intro to Hardware Memory Consistency
\end{closeenum}
}

\section{Final Project: Logistics}

The final project is an opportunity for you to work on a project of your own choosing.  Highly recommended to talk to Professor Lee before you decide on a project.

\subsection{Possible Topics}

Some possible ideas for the final project are listed below.

\begin{itemize}
\item You can choose anything that relates to research, but small enough that you have deliverable in 4 weeks.
\item You can implement the solution to a problem in Cilk Plus.
\item You can implement concurrent data structures and evaluate their performance.  We will talk about concurrent data structures in the next lectures.  Can use Java, C++, or any language you like.
\item Literature search on a specific topic that interest you.  Talk to Angelina before you go down this road.
\item Build a tool to measure some aspect of your code, like the Project 3 analyzer but to measure cache miss, instruction count, etc.
\end{itemize}

\subsection{Timeline and Logistics}

\begin{itemize}
\item You have about 4 weeks; 3 weeks until presentation
\item Work in a pair of two (recommended)
\item A 20-25 minutes presentation (during the last two lectures); it is okay for a project to be a work in progress.  You should be able to state the problems you are solving, and the current status.
\item A writeup of around 5 pages (single writeup, but in the writeup, specify how responsibilities are divided)
\end{itemize}

\section{Sequential Consistency (SC)}

\textbf{Sequential consistency}, which is typically used as a memory consistency model, relates order of memory operations across different processors.  It is a framework that lets us reason about acceptable behavior of a program.

Given a parallel computation, it may be possible to have multiple different outputs.  SC precludes an execution order that doesn't conform to program order.

You can compare this to the execution DAG, as shown in Figure~\ref{fig:sc1}.  SC tells us that \textit{if a program is sequentially consistent}, then any result from that program must be able to have been created by a topological sort of the nodes in the execution DAG.

The concept of SC is quite important.  When people talk about things like locking algorithms, concurrent data structures, and so on, you reason about the correctness of your code assuming that SC holds.  (Note that the hardware is implemented in a way such that SC is not necessarily true.)

\begin{figure*}[h!]
  \centering
      \includegraphics[width=1\textwidth]{sequential_consistency_intro}
  \caption{Example of how the idea of sequential consistency can be illustrated using a DAG.}
  \label{fig:sc1}
\end{figure*}

\section{Linearizability}

SC was originally designed to reason about memory.  \textbf{Linearizability} was originally developed to reason about data structures.

\subsection{Circular FIFO Queue Example}

\begin{wrapfigure}{r}{0.5\textwidth}
	\begin{center}
		\includegraphics[width=2in]{circle_queue}
	\end{center}
	\caption{Illustration of the circular queue.}
	\label{fig:circle}
	\vspace{-30pt}
\end{wrapfigure}

Consider a circular FIFO queue, as shown in Figure~\ref{fig:circle}.  It is a FIFO queue implemented using a ``circular'' data structure, where a finite number \textit{capacity} of elements can be stored in the queue.  The tail pointer continually goes around the circle pushing new elements, and the head pointer follows it when popping elements.  If the tail ``catches up'' with the head, then the queue is full.  If the head ``catches up'' with the tail, then the queue is empty.

\paragraph{Claim} The serial implementation can be used in a two-processor implementation, where one thread always calls enqueue (but not dequeue), and the other thread always calls dequeue (but not enqueue).

\paragraph{Problem} How do we reason about the correctness of the above use case?

\paragraph{Reasoning}
You can try thinking in terms of sequential (serial) specifications.  With sequential reasoning, you only care about how the state changes between the start and end of a function call.  However, when we have concurrency, we need to care about the state in the middle of when a method call is occurring.

\subsection{Linearization}

\begin{figure*}[h!]
  \centering
      \includegraphics[width=1\textwidth]{linearizable}
  \caption{Illustration of how we can choose linearization points within four intervals to make a program execution linearizable.}
  \label{fig:lin}
\end{figure*}

Map each memory action to an instant in time, called the \textit{linearization point}.  Given an execution history (the time intervals during which method invocations occur on each thread), that history is \textit{linearizable} if we can select a linearization point within each method call that is consistent with the program execution; see Figure~\ref{fig:lin} for an example.  When you think about linearizability, you need to consider the execution history.

It is possible for there to be multiple valid linearizations.  See slides for an example.

\paragraph{Formal Definition} A history $H$ is \textit{linearizable} if it can be extended to G by appending zero or more responses to pending invocations and discarding other pending invocations such that G is equivalent to a legal sequential history $S$ such that the partial order in G is a subset of the sequential history S.

\paragraph{Connecting to Sequential Consistency}
SC is weaker than linearizability.  One can come up with a trivial example of a history that is sequentially consistent but not linearizable.  See slides for one such example.

\paragraph{Composability Theorem}
A history $H$ is linearizable iff, for every object $x$, $H|x$ is linearizable.

Note that SC is not composable (this theorem does not hold for SC).

\section{Hardware Memory Consistency}

No modern-day processors implement sequential consistency.  Hardware actively reorders instructions.  Compilers might also violate sequential consistency.  Why?  Performance.

A common alternative for hardware is a \textit{memory fence}.

\subsection{Summary}

Hardware implements a weaker protocol than sequential consistency.  We can get sequential consistency, but at a price.  Linearizability is a better fit for high-level software.

\end{document}